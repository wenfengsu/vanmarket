/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import User from '../api/user/user.model';
import Product from '../api/product/product.model';
import Catalog from '../api/catalog/catalog.model';

var mainCatalog, home, books, clothing;

User.find({}).remove()
  .then(() => {
    User.create({
      provider: 'local',
      name: 'Test User',
      email: 'test@example.com',
      password: 'test'
    }, {
      provider: 'local',
      role: 'admin',
      name: 'Admin',
      email: 'admin@example.com',
      password: 'admin'
    })
    .then(() => {
      console.log('finished populating users');
    });
  });

/*Product.find({}).remove()
  .then(function(){
    Product.create({
      title: '2010 Hoda Civic',
      imageUrl: '/assets/uploads/2010hondacivic.jpg',
      price: 9980,
      stock: 1,
      description: 'Like New condition, please contact for viewing'
    },{
      title: 'Free Range Chicken',
      imageUrl: '/assets/uploads/chicken.jpg',
      price: 8,
      stock: 340,
      description: 'fresh local chicken'
    },{
      title: 'Coffee beans',
      imageUrl: '/assets/uploads/coffeebean.jpg',
      price: 15,
      stock: 60,
      description: 'fresh baked coffee bean'
    },{
      title: 'Home made cheese bread',
      imageUrl: '/assets/uploads/cheesebread.jpg',
      price: 2,
      stock: 20,
      description: 'fresh baked coffee bean'
    })
    .then(()=>{
      console.log('finished populating products');
    });
   });
*/

Catalog
  .find({})
  .remove()
  .then(function(){
    return Catalog.create({name: 'All'});
  })
  .then(function(catalog){
    mainCatalog = catalog;
    return mainCatalog.addChild({name: 'Home'});
  })
  .then(function(category){
    home = category._id;
    return mainCatalog.addChild({name: 'Books'});
  })
  .then(function(category){
    books = category._id;
    return mainCatalog.addChild({name: 'Clothing'});
  })
  .then(function(category){
   clothing = category._id;
   return Product.find({}).remove({});
  })
  .then(function(){
    return Product.create({
	  title: 'My eBook',
	  imageUrl: '/assets/uploads/ebook.jpg',
	  price: 25,
	  stock: 250,
	  categories: [books],
	  description: 'Build a ecommerce app MEAN stack app'
	},{
	  title: 'tShirt',
	  imageUrl: '/assets/uploads/tshirt.jpg',
	  price: 15,
	  stock: 100,
	  categories: [clothing],
	  description: 'this is a new tshirt'
	},{
	  title: 'Coffee bean',
	  imageUrl: '/assets/uploads/coffeebean.jpg',
	  price: 8,
	  stock: 50,
	  categories: [home],
	  description: 'get the best coffee ever'
	});
  })
  .then(function (){
    console.log('Finished populating Products with categories');
  })
  .then(null, function (err){
    console.error('Error populating Products & categories: ', err);
  });

