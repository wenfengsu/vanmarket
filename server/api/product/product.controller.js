/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/products              ->  index
 * POST    /api/products              ->  create
 * GET     /api/products/:id          ->  show
 * PUT     /api/products/:id          ->  update
 * DELETE  /api/products/:id          ->  destroy
 */

'use strict';

var path = require('path');

import _ from 'lodash';
import Product from './product.model';
import Catalog from '../catalog/catalog.model';

function responseWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}


function saveFile(res, file){
	return function(entity){
	  var newPath = '/assets/uploads/'+path.basename(file.path);
	  entity.imageUrl = newPath;
	  return entity.save()
	    .then(updated =>{
	      return updated;
	    });
	}
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}


function productsInCategory(catalog) {
  var catalog_ids = [catalog._id].concat(catalog.children);
  return Product
    .find({'categories': { $in: catalog_ids } })
    .populate('categories')
    .exec();
}

// Gets a list of Products
export function index(req, res) {
  return Product.find().exec()
    .then(responseWithResult(res))
    .catch(handleError(res));
}

// Gets a single Product from the DB
export function show(req, res) {
  return Product.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(responseWithResult(res))
    .catch(handleError(res));
}

// Creates a new Product in the DB
export function create(req, res) {
  return Product.create(req.body)
    .then(responseWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing Product in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return Product.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(responseWithResult(res))
    .catch(handleError(res));
}

// Deletes a Product from the DB
export function destroy(req, res) {
  return Product.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}

export function upload(req, res) {
	var file = req.files.file;
	if(!file){
		return handleError(res)('File not provided');
	}
	return Product.findById(req.params.id).exec()
		.then(handleEntityNotFound(res))
		.then(saveFile(res, file))
		.then(responseWithResult(res))
		.catch(handleError(res));
}

export function catalog (req, res) {
  Catalog
    .findOne({ slug: req.params.slug })
    .exec()
    .then(productsInCategory)
    .then(responseWithResult(res))
    .catch(handleError(res));
};


export function search(req, res){
	Product.find({$text: {$search: req.params.term}})
	  .populate('categories')
	  .exec(function (err, products){
	    if(err) {return handleError(res, err);}
	    return res.json(200, products);
	});
};
