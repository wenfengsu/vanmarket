'use strict';

var _=require('lodash');
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var Schema = mongoose.Schema;
var Braintree = require('../braintree/braintree.model');

var OrderDetailsSchema = new Schema({
	product: {type: Schema.Types.ObjectId, ref: 'Product'},
	quantity: Number,
	total: {type:Number, get: getPrice, set: setPrice}
});

var OrderSchema = new Schema({
	// buyer details
	name: String,
	user: {type: Schema.Types.ObjectId, ref: 'User' },
	shippingAddress:String,
	billingAddress: String,

	//price details
	items: [OrderDetailsSchema],
	shipping: {type:Number, get: getPrice, set: setPrice, default: 0.0},
	tax: {type: Number, get: getPrice, set: setPrice, default: 0.0},
	discount: {type: Number, getPrice, set: setPrice, default: 0.0},
	subTotal: {type: Number, get: getPrice, set: setPrice},
	total: {type: Number, required: true/*, get:getPrice, set: setPrice*/},

	//payment info
	status: {type: String, default: 'pending'}, //pending, paid/failed, delivered, canceled, refunded.
	paymentType: {type: String, default: 'braintress'},
	nonce: String,
	type: String
});

OrderSchema.pre('validate', function (next){
	if(!this.nonce){return next();}
	executePayment(this, function (err, result){
		this.pamentStatus = result;
		if(err || !result.errors){
			this.statud = 'failed.'+result.errors+err;
			next(err || result.errors);
		}else{
			this.status = 'pasid';
			next();
		}
	
	}.bind(this));
});


function executePayment(payment, cb){
	Braintree.transaction.sale({
		amount: payment.total,
		paymentMethodNonce: payment.nonce,
	}, cb);
}

function getPrice(num){
	return parseFloat((num/100).toFixed(2));
}

function setPrice(num){
	return num * 100;
}

module.exports = mongoose.model('Order', OrderSchema);



/*import mongoose from 'mongoose';

var OrderSchema = new mongoose.Schema({
  name: String,
  info: String,
  active: Boolean
});

export default mongoose.model('Order', OrderSchema);
*/
