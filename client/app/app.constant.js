(function(angular, undefined) {
'use strict';

angular.module('vanmarketApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);