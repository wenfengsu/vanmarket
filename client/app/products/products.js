'use strict';

angular.module('vanmarketApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('products', {
        url: '/products',
        template: '<products></products>'
      })

      .state('productNew', {
        url: '/products/new',
        template: '<product-new></product-new>'
      })

      .state('productDetail', {
        url: '/products/:id/detail',
        template: '<product-detail></product-detail>'
      })

      .state('productEdit', {
        url: '/products/:id/edit',
        template: '<product-edit></product-edit>'
      })

      .state('productsCheckout', {
	url:'/checkout',
	template: '<products-checkout></products-checkout>'
      })

      .state('productsCatalog', {
	url:'/products/:slug/catalog',
	template: '<products-catalog></products-catalog>'
      });

  });
