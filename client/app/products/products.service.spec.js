'use strict';

describe('Service: ProductService', function () {

  // load the service's module
  beforeEach(module('vanmarketApp'));
  beforeEach(module('stateMock'));
  beforeEach(module('socketMock'));

  // instantiate service
  var ProductService,
	$httpBackend,
	validAttributes = [
		{title: 'Product1', price: 123.45},
		{title: 'Product2', price: 678.90}
	],
	newAttributes = {title: 'Product3', price: 1000},
	productWithId = angular.extend({}, newAttributes, {id:123});
	


  beforeEach(inject(function (_ProductService_, _$httpBackend_) {
    ProductService = _ProductService_;
    $httpBackend = _$httpBackend_;

  }));

  describe('#get all product service', function(){
	it('should fetch products with HTTP GET request', function(){
		$httpBackend.expectGET('/api/products').respond(validAttributes);
		ProductService.query(function (products){
			expect(products).to.equal(validAttributes);
		});
	});

	it('should work with empty data', function(){
		$httpBackend.expectGET('/api/products').respond([]);
		ProductService.query(function (products){
			expect(products).to.equal([]);
		});
	});
  });

  describe('#get one product service', function(){
	it('should get a single product by id', function(){
		$httpBackend
			.expectGET('/api/products/1')
			.respond(validAttributes[0]);
		ProductService.get({id:1}, function(product){
			expect(product).to.equal(validAttributes[0]);
		});
	});
  });

  describe('#create product service', function(){
	beforeEach(function(){
		$httpBackend
			.expect('POST', '/api/products', JSON.stringify(newAttributes))
			.respond(productWithId);
	});
	it('should create a new product from the class', function(){
		var newProduct = ProductService.save(newAttributes, successCb(productWithId));
		expect(toJson(newProduct)).to.eql(newAttributes);
	});
	it('should create a new product from the instance', function(){
		var product = new ProductService();
		product.title = 'Product3';
		product.price = 1000;
		product.$save(successCb(productWithId));
		expect(toJson(product)).to.eql(newAttributes);
	});

  });

  describe('#update product service test', function(){
	var updatedValues = {title: 'new title', price: 987};
	it('should update attributes with PUT', function(){
		$httpBackend
			.expectPUT('api/products/123', updatedValues)
			.respond(angular.extend({}, updatedValues, {id:123}));

		ProductService.update({id:12}, updatedValues, function(product){
			expect(product.id).to.be(123);
			expect(product.title).to.be('new title');
			expect(product.price).to.be(987);
		});
	});
  });

  describe('#delete product service test', function(){
	it('should delete product', function(){
		$httpBackend
			.expectDELETE('api/products/123')
			.respond({});
		ProductService.delete({id:123}, successCb);
		ProductService.remove({id:123}, successCb);
	});;
  });

  function toJson(obj){
  	return JSON.parse(JSON.stringify(obj));
  }

  function successCb(match){
	return function(value/*, responseHeaders*/){
		console.log(value+'*****'+match);
		expect(value.to.equal(match));
	};
  }


});

