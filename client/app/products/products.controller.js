'use strict';

var errorHandler, uploadHandler;

(function(){

class ProductsController {
  constructor(ProductService, $scope) {
    $scope.products = ProductService.query();
    $scope.$on('search:term', function (event, data){
	if(data.length){
		$scope.products = ProductService.search({id:data});
		$scope.query = data;
	}else{
		$scope.products = ProductService.query();
		$scope.query = '';
	}
    });
  }
}

class ProductNewController {
  constructor(ProductService, $scope, $state) {
    $scope.product = {};
    $scope.addProduct= function(){

	return ProductService.save($scope.product).$promise.then(function (product){
		return ProductService.upload($scope.product.picture, product._id);
	}).then(function(product){
		$state.go('productDetail', {id: product._id});
	}).catch(errorHandler($scope));
    };
  }
}


class ProductsCatalogController{
  constructor(ProductService, $scope, $stateParams){
    $scope.products = ProductService.catalog({id: $stateParams.slug});
    $scope.query = $stateParams.slug;
  }

}


class ProductDetailController{
  constructor(ProductService, $scope, $state, $stateParams){

    $scope.product = ProductService.get({id:$stateParams.id});

    $scope.deleteProduct = function(){
        ProductService.delete({id: $scope.product._id}, function success(/*value, respnonseHeaders*/){
		$state.go('products');
	}, errorHandler($scope));
    };
  }
}

class ProductEditController{
  constructor(ProductService, $scope, $state, $stateParams, Upload, $timeout){

    $scope.product = ProductService.get({id: $stateParams.id});
    $scope.editProduct = function(){

	return ProductService.update({id: $scope.product._id}, $scope.product).$promise.then(function (product){

                return ProductService.upload($scope.product.picture, product._id);

        }).then(function(product){

                $state.go('productDetail', {id: product._id});

        }).catch(errorHandler($scope));
    };

    $scope.upload = uploadHandler($scope, Upload, $timeout);
  }
}



class ProductsCheckoutController{
  constructor($scope, $state, $http, ngCart){
	// checkout products here
	$scope.errors = '';
	$scope.paymentOptions = {
		onPaymentMethodReceived: function(payload){
			angular.merge(payload, ngCart.toObject());
			payload.total = payload.totalCost;
			$http.post('/api/orders', payload)
			.then(function success(){
				ngCart.empty(true);
				$state.go('products');
			}, function error(res){
				$scope.errors = res;
			});
		}
	};
  }
}

errorHandler = function ($scope){
  return function error(httpResponse){
    console.log('failed: ', httpResponse);
    $scope.errors = httpResponse;
  };
};

uploadHandler = function($scope, Upload, $timeout){
  return function(file){
	if(file && !file.$error){
		$scope.file = file;
		file.upload = Upload.upload({
			url	: '/api/products/'+$scope.product._id+'/upload', 
			data	: {file	:file}
		});
		file.upload.then(function(response){
			$timeout(function(){
				file.result = response.data;
			});
		}, function(response){
			if(response.status>0){
				errorHandler($scope)(response.status+'bbb:'+response.data);
			}
		});
		file.upload.progress(function(evt){
			file.progress=Math.min(100, parseInt(100.0 * evt.loaded/evt.total));
		});
	}

   };
};


angular.module('vanmarketApp')
  .component('products', {
    templateUrl: 'app/products/templates/products-list.html',
    controller: ProductsController
  })
  .component('productNew', {
    templateUrl: 'app/products/templates/product-new.html',
    controller: ProductNewController
  })
  .component('productDetail', {
    templateUrl: 'app/products/templates/product-detail.html',
    controller: ProductDetailController
  })

  .component('productEdit', {
    templateUrl: 'app/products/templates/product-edit.html',
    controller: ProductEditController
  })

  .constant('clientTokenPath', '/api/braintrees/client_token')
  .component('productsCheckout', {
    templateUrl: 'app/products/templates/products-checkout.html',
    controller: ProductsCheckoutController
  })

  .component('productsCatalog', {
    templateUrl: 'app/products/templates/products-list.html',
    controller: ProductsCatalogController
  });

})();
