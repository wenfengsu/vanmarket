'use strict';

describe('Component: products', function () {


  var component, scope, ProductService, mockProduct, state, $componentController, $q, stateParams;
  var validAttributes = [
	{_id: 1, title: 'Product 1', price: 100.10, stock: 10},
	{_id: 2, title: 'Product 2', price: 200.20, stock: 10}
  ];

  var stateparams = {slug: 'home'};
  

  // load the controller's module
  beforeEach(module('vanmarketApp'));


  // Initialize the controller and a mock scope
  beforeEach(inject(function (/*_$componentController,*/ $rootScope, _ProductService_, _$q_) {
    scope = $rootScope.$new();

    mockProduct = validAttributes[0];
    ProductService = _ProductService_;
    stateParams = stateparams;

    sinon.stub(ProductService, 'query').returns(validAttributes);
    sinon.stub(ProductService, 'get').returns(mockProduct);
    sinon.stub(ProductService, 'catalog').returns(validAttributes);

    $q = _$q_;

    state = {go: sinon.stub()};
  }));


  describe('ProductsComponent', function(){

	beforeEach(inject(function($componentController){
		$componentController('products', {
			$scope: scope,
			ProductService: ProductService,
		});
	}));

	it('should have valid products', function(){
		expect(scope.products).to.eql(validAttributes);
	});
  });

  describe('ProductDetailComponent', function(){
	beforeEach(inject(function($componentController){
		$componentController('productDetail', {
			$scope: scope,
			ProductService: ProductService,
			$state: state,
		});
	}));
	
	it('should get a single product', function(){
		expect(scope.product).to.eql(mockProduct);
	});

	it('should delete the product and redirect if successed', function(){
		var stub = sinon.stub(ProductService, 'delete', callCallbackWithError());
		scope.deleteProduct();
		state.go.should.have.been.calledWith('products');
		assert(stub.withArgs({id:mockProduct._id}).calledOnce);
	});
	it('should not redirect if delete failed with errors', function(){
		sinon.stub(ProductService, 'delete', callCallbackWithError(true));
		scope.deleteProduct();
		expect(state.go.calledOnce).to.equal(false);
	}); 

  });


  describe('ProductsNewComponent', function(){
	beforeEach(inject(function ($componentController){
		$componentController ('productNew', {
			$scope: scope,
			ProductService: ProductService,
			$state: state,
		});
	}));

	it('should create a new product and redirect to all products', function(done){
		var d = $q.defer();
		var productSave = sinon.stub(ProductService, 'save').returns({$promise: d.promise});
		var productUpload = sinon.stub(ProductService, 'upload').returns(d.promise);
		scope.product = mockProduct;
		scope.product.picture = 'mock-picture';
		scope.addProduct().then(function(){
			assert(productSave.withArgs(mockProduct).calledOnce);
			assert(productUpload.withArgs(scope.product.picture, mockProduct._id).calledOnce);
			state.go.should.have.been.calledWith('productDetail', {id: mockProduct._id});
			done();
		});
		d.resolve(mockProduct);
		scope.$digest();
	});
	it('should not redirect if save fails', function(done){
		var d = $q.defer();
		var productSave = sinon.stub(ProductService, 'save').returns({$promise:d.promise});
		scope.product = mockProduct;
		scope.addProduct().then(function (){
			assert(productSave.withArgs(mockProduct).calledOnce);
			expect(state.go.calledOnce).to.equal(false);
			done();
		});
		d.reject();
		scope.$digest();
	});
  });

  describe('ProductsCatalogComponent', function(){
	beforeEach(inject(function ($componentController){
		$componentController ('productsCatalog', {
			$scope: scope,
			ProductService: ProductService,
			$state: state,
			$stateParams: stateParams
		});
	}));
	it('should show the correct products under the correct categories', function(){
		expect(scope.products).to.be.eql(validAttributes);
		expect(scope.query).to.equal(stateparams.slug);
	});

  });

  describe('ProductsEditComponent', function(){
	beforeEach(inject(function ($componentController){
		$componentController ('productEdit', {
			$scope: scope,
			ProductService: ProductService,
			$state: state
		});
		scope.product = mockProduct;
	}));
	it('should edit product and redirect to view if success', function(done){
		var d = $q.defer();
		var productUpdate = sinon.stub(ProductService, 'update').returns({$promise: d.promise});
		var productUpload = sinon.stub(ProductService, 'upload').returns(d.promise);
		scope.product = mockProduct;
		scope.product.picture = 'mock-picture-updated';
		scope.editProduct().then(function(){
			assert(productUpdate.calledOnce);
			assert(productUpload.withArgs(scope.product.picture, mockProduct._id).calledOnce);
			state.go.should.have.been.calledWith('productDetail', {id:mockProduct._id});
			done();
		});
		d.resolve(mockProduct);
		scope.$digest();
	});

	it('should not redirect if failed', function(done){
		var d = $q.defer();
		var productUpdate = sinon.stub(ProductService, 'update').returns({$promise: d.promise});
		scope.product = mockProduct;
		scope.editProduct().then(function (){
			assert(productUpdate.calledOnce);
			expect(state.go.calledOnce).to.equal(false);
			done();
		});
		d.reject();
		scope.$digest();
	});
  });


  function callCallbackWithError(err){
    return function(/* arguments */){
      var length    = arguments.length;
      var errorCb   = arguments[length-1];
      var successCb = arguments[length-2];
      var params    = arguments[length-3];

      if(err){
        return errorCb(err);
      } else {
        return successCb(params);
      }
    };
  }

});
