'use strict';

angular.module('vanmarketApp.admin', [
  'vanmarketApp.auth',
  'ui.router'
]);
