'use strict';

angular.module('vanmarketApp', [
  'vanmarketApp.auth',
  'vanmarketApp.admin',
  'vanmarketApp.constants',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'btford.socket-io',
  'ui.router',
  'ui.bootstrap',
  'ngFileUpload',
  'validation.match',
  'ngCart',
  'braintree-angular'
])
  .config(function($urlRouterProvider, $locationProvider) {
    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(true);
  });
