'use strict';

angular.module('vanmarketApp.auth', [
  'vanmarketApp.constants',
  'vanmarketApp.util',
  'ngCookies',
  'ui.router'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
