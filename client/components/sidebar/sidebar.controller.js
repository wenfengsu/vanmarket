'use strict';



class SidebarController {

  constructor(Auth, $scope, Catalog, $location) {
    $scope.catalog = Catalog.query();

    $scope.isActive = function(route){
      return $location.path().indexOf(route) > -1;
    };

  }
}

angular.module('vanmarketApp')
  .controller('SidebarController', SidebarController);
