'use strict';

angular.module('vanmarketApp')
  .factory('Catalog', function($resource){
	return $resource('/api/catalogs/:id');
  });
